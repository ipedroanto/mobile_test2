import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, ApplicationSettings } from "@nativescript/core";
import { Router } from "@angular/router";
import { RouterExtensions } from "@nativescript/angular";

@Component({
    selector: "Username",
    moduleId: module.id,
    // templateUrl: "./settings.username.html"
    template:
    `<TextField #texto="ngModel" [(ngModel)]="textFieldValue" hint="Ingresar nombre de usuario" required minlen="3"></TextField>
    <Label *ngIf="texto.hasError('required')" text="*"></Label>
    <Label *ngIf="!texto.hasError('required') && texto.hasError('minlen')" text="3+"> </Label>

    <Button (tap)="onNavItemTap('/settings')" class="btn">
            <FormattedString>
                <Span text="&#xf00d;" class="fas"></Span>
                <Span text=" Cancelar"></Span>            
            </FormattedString>
        </Button>
    
        <Button (tap)="onButtonTap()" class="btn">
            <FormattedString>
                <Span text="&#xf00c;" class="fas"></Span>
                <Span text=" Aceptar"></Span>            
            </FormattedString>
        </Button>`
})
export class SettingsUsername implements OnInit {
    textFieldValue: string = "";
    @Output() search1: EventEmitter<string> = new EventEmitter();
    @Input() inicial: string;
    constructor(private router: Router, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        if( ApplicationSettings.hasKey("nombreUsuario") ) {
            this.textFieldValue = ApplicationSettings.getString("nombreUsuario");
        }
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.closeDrawer();
    }

    onButtonTap(){
        this.guardarUserName(this.textFieldValue);
    }

    guardarUserName(s: string){
        //console.log("guardarUserName: " + s);
        ApplicationSettings.setString("nombreUsuario", s);
        this.onNavItemTap('/settings');
    }
}