import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { SettingsComponent } from "./settings.component";
import { SettingsUsername } from "./settings.username";

const routes: Routes = [
    { path: "", component: SettingsComponent },
    { path: "username", component: SettingsUsername }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SettingsRoutingModule { }
